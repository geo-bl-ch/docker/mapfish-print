.DEFAULT_GOAL := render

TEMPLATES = $(shell find /usr/local/tomcat -name '*.tpl')

%: %.tpl
	perl -p -e 's/\$$\{([^}]+)\}/defined $$ENV{$$1} ? $$ENV{$$1} : $$&/eg' < $< > $@
	rm -f $<

.PHONY: render
render: $(TEMPLATES:.tpl=)
