Mapfish Print
=============

This image provides a [Mapfish Print](http://mapfish.github.io/mapfish-print-doc/) instance
served by [Tomcat](http://tomcat.apache.org/).

Usage
-----

Extend this image to include your print app. The print app has to be copied into the
`/usr/local/tomcat/webapps` folder.

```dockerfile
FROM registry.gitlab.com/gf-bl/docker/mapfish-print

COPY --chown=1001:0 path/to/my/print-app /usr/local/tomcat/webapps/ROOT/print-apps
```

You could also mount a volume to `/usr/local/tomcat/webapps/ROOT/print-apps` containing the print app.

The environment variable **JAVA_OPTS** defaults to:

```bash
JAVA_OPTS="-Djava.awt.headless=true -Dfile.encoding=UTF-8"
```

If you overwrite **JAVA_OPTS** with custom values, please be sure to keep these default values.

Templates
---------

You can use templates which are rendered at container start up. These templates have to end with
`*.tpl`. For example, `config.yaml.tpl` will be rendered to `config.yaml`.

Within the templates, you can access environment variables using `${MY_VARIABLE}`. Please note,
that the **variable names are case-sensitive**.
